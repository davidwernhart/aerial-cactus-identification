import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.keras.backend as K
import math
import cv2
import os
from Model import create_model, create_model_dynamic
import datetime
from random import randint
from random import seed
from random import uniform
import json
from sklearn.metrics import f1_score, recall_score, precision_score
from sklearn.model_selection import StratifiedKFold

def create_dict(s):
    #seed(s)
    seed()
    dictionary = {}
    filter_num = {0: 4, 1: 8, 2: 16, 3: 32, 4: 64, 5: 64}
    for i in range(10):
        r = randint(0, 5) #number of fully connected layers
        conv_num = randint(1, 3)
        topology = [] #stores number of neurons for each dense layer
        conv_top = [] #stores number of neurons for each conv layer
        pool_top = [] #stores the max pooling size after each conv layer
        lr = 0.0001 #this gets overwritten in first loop
        for j in range(0, r):
            r2 = randint(2, int(2500/r)) #more layers means smaller number of neurons
            topology.append(r2)
            lr = float(format(uniform(0.01, 0.0001), '.4f'))
        for j in range(0, conv_num):
            n = j+2
            filter_n = filter_num[randint(0, (n)%6)]
            conv_top.append(filter_n)
            max_pool = randint(1, 2)
            pool_top.append(max_pool)
        dictionary[i] = {"conv": conv_num, "conv_top": conv_top, "pool_top": pool_top, "fully": r, "top": topology, "lr": lr}
        
    #dictionary[1] = {"conv": 3, "conv_top": [16, 32, 64], "pool_top": [2, 2, 2], "fully": 1, "top": [512], "lr": 0.001}
    print(dictionary)
    return dictionary

def train_test_loop(X, y):
    directory = "..\\logs\\fit\\"
    model_dir = "..\\models\\"
    if not os.path.exists(directory):
        os.makedirs(directory)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    s = 1234
    topology_dict = create_dict(s)
    d_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    for i in range(0, len(topology_dict)):
        print("Topology: "+str(i))
        #create new model
        model = create_model_dynamic(X, topology_dict[i], s)
        
        log_dir= directory+"topology_"+str(i)+"_"+ d_time
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        acc, loss, f1, precision, recall = k_fold_cross_validation(model, X, y, tensorboard_callback)
        model.save(model_dir+"topology_"+str(i)+"_"+d_time+".h5")
        topology_dict[i]["acc"] = acc
        topology_dict[i]["loss"] = loss
        topology_dict[i]["f1_score"] = f1
        topology_dict[i]["precision"] = precision
        topology_dict[i]["recall"] = recall
        file = model_dir+"topology_"+str(i)+"_"+d_time+".json"
        with open(file, "w") as f:
            f.write(json.dumps(topology_dict[i]))
        print("acc:", acc)
        print("loss:", loss)
        K.clear_session()
    summary_file = model_dir+"summary_"+d_time+".json"
    with open(summary_file, "w") as f:
            f.write(json.dumps(topology_dict))
def k_fold_split(n):
    kf = StratifiedKFold(n_splits=n, shuffle=True)
    return kf
def split_data(X, y, train_size):
    train_size = math.floor(train_size * len(y))
    X_train, y_train = X[:train_size], y[:train_size]
    X_test, y_test = X[train_size:], y[train_size:]
    trues = 0
    falses = 0
    for label in y_train:
        if label == 1:
            trues = trues+1
        else:
            falses = falses+1
    print("Trues: "+str(trues))
    print("Falses: "+str(falses))
    return X_train, y_train, X_test, y_test


def quick_eval(model, X, y, train_size=0.8, epochs=5):
    X_train, y_train, X_test, y_test = split_data(X, y, train_size)

    print("starting to train the network...")
    model.fit(X_train, y_train, epochs=epochs)

    print("done training! starting to evaluate...")
    loss, acc, f1_score, precision, recall = model.evaluate(X_test, y_test)

    return loss, acc

def reset_weights(model):
    session = K.get_session()
    for layer in model.layers:
        if hasattr(layer, 'kernel_initializer'):
            layer.kernel.initializer.run(session=session)
        if hasattr(layer, 'bias_initializer'):
            layer.bias.initializer.run(session=session)

def k_fold_cross_validation(model, X, y, tensorboard_callback=None, k=10, epochs=5):
    train_size = 1 - (1 / k)
    print("train_size", train_size)
    avg_acc = 0
    avg_loss = 0
    kf = k_fold_split(k)
    
    history_array = []
    X_predict = []
    y_true = []
    if tensorboard_callback != None:
        log_dir = tensorboard_callback.log_dir
    
    for i in range(0, k):
        X_train_index, X_test_index = next(kf.split(X,y))
        X_train, X_test = X[X_train_index], X[X_test_index]
        y_train, y_test = y[X_train_index], y[X_test_index]
        ones = 0
        zeroes = 0
        for label in y_train:
            if label == 1:
                ones += 1
            else:
                zeroes += 1
            
        print("Ones: "+str(ones))
        print("Zeroes: "+str(zeroes))
        print("k:", i)
        reset_weights(model)
        #X_train, y_train, X_test, y_test = split_data(X, y, train_size)
        print("starting to train the network...")
        if tensorboard_callback != None:    
            tensorboard_callback.log_dir = log_dir+"_"+str(i)
            history_array.append(model.fit(X_train, y_train, epochs=epochs, callbacks=[tensorboard_callback]))
            print(model.summary())
        else:
            history_array.append(model.fit(X_train, y_train, epochs=epochs))
        print("done training! starting to evaluate...")
        loss, acc = model.evaluate(X_test, y_test)
        
        y_true = y_test
        X_predict = X_test
        avg_acc += acc
        avg_loss += loss

    y_pred = model.predict_classes(X_predict)
    prec = precision_score(y_true, y_pred, average='binary')
    rec = recall_score(y_true, y_pred, average='binary')
    f1 = f1_score(y_true, y_pred, average='binary')
    print("PRECISION: "+str(prec))
    print("RECALL: "+str(rec))
    print("F1SCORE: "+str(f1))
    avg_loss = avg_loss / k
    avg_acc = avg_acc  / k
    #if tensorboard_callback == None:
     #   plt.plot(history_array[0].history['acc'])
      #  plt.show()
    return avg_acc, avg_loss, f1, prec, rec


#train CNN:
#https://www.kaggle.com/abhinand05/in-depth-guide-to-convolutional-neural-networks
#https://www.tensorflow.org/tutorials/images/cnn

traindf = pd.read_csv("dataset/train.csv")

print("setting up CNN...")

Xrgb = []
Xr = []
Xg = []
Xb = []
y = []

print("loading...")


positiveLabels = 0
negativeLabels = 0
for index, row in traindf.iterrows():
    id = row.get("id")
    label = row.get("has_cactus")
    image = cv2.imread(("dataset/train/"+id))
    blue = np.expand_dims(np.asarray(cv2.extractChannel(image,0)),3)
    green = np.expand_dims(np.asarray(cv2.extractChannel(image, 1)), 3)
    red = np.expand_dims(np.asarray(cv2.extractChannel(image, 2)), 3)
    """
    if (positiveLabels >= negativeLabels and label == 0) or (positiveLabels <= negativeLabels and label == 1):
        Xr.append(red)
        Xg.append(green)
        Xb.append(blue)
        Xrgb.append(np.asarray(image))
        y.append(label)

        if label == 1:
            positiveLabels += 1
        elif label == 0:
            negativeLabels += 1
    """
    Xr.append(red)
    Xg.append(green)
    Xb.append(blue)
    Xrgb.append(np.asarray(image))
    y.append(label)

Xrgb = np.array(Xrgb)
Xr = np.array(Xr)
Xg = np.array(Xg)
Xb = np.array(Xb)
y = np.array(y)

print("done loading data!")

trues = 0
falses = 0
for label in y:
    if label == 0:
        falses = falses +1
    elif label == 1:
        trues = trues +1
    else:
        print("Not labels"+str(label))
print("Total: "+str(len(y)))
print("True: "+str(trues))
print("False: "+str(falses))



#quick_eval(model, X, y)
'''
#model = create_model(Xrgb)
model = create_model(Xrgb)
acc, loss, f1, precision, recall = k_fold_cross_validation(model, Xrgb, y)

print("acc:", acc)
print("loss:", loss)
print("fscore:",f1)
'''
train_test_loop(Xrgb, y)


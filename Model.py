import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.keras.backend as K
import cv2

   
def recall_m(y_true, y_pred):
    """Recall metric.
    Only computes a batch-wise average of recall.
    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    """Precision metric.
    Only computes a batch-wise average of precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall +K.epsilon()))

def create_model(trainingData):
    print("input shape: " + str(trainingData[0].shape))
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv2D(16, (3, 3), activation='relu', input_shape=trainingData[0].shape))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(512, activation='relu'))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))

    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    return model


def create_model_dynamic(trainingData, topology_dict, s):
    pool = topology_dict["pool_top"]
    conv_num = topology_dict["conv"]
    conv_top = topology_dict["conv_top"]
    top = topology_dict["top"]
    fully = topology_dict["fully"]
    lr = topology_dict["lr"]
    if len(pool) != len(conv_top):
        raise ValueError('Mismatch between number of conv layers and number of pooling layers.')
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv2D(conv_top[0], (3, 3), activation='relu', input_shape= trainingData[0].shape))
    for i in range(1, conv_num):  
        model.add(tf.keras.layers.Conv2D(conv_top[i], (3, 3), activation='relu'))
        model.add(tf.keras.layers.MaxPooling2D((pool[i], pool[i])))
    model.add(tf.keras.layers.Flatten())
    for i in range(0, fully):
        model.add(tf.keras.layers.Dense(top[i], activation='relu'))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))

    adam = tf.keras.optimizers.Adam(lr=lr)
    model.compile(optimizer=adam,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    return model
